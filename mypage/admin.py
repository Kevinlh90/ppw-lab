from django.contrib import admin
from .models import Status, Profile

# Register your models here.
class StatusModelAdmin(admin.ModelAdmin):
    list_display = ["text","id"]
    list_filter = ["text", "id"]
    
    class Meta:
        model = Status
        
class ProfileModelAdmin(admin.ModelAdmin):
    list_display = ["username","email","password","id"]
    list_filter = ["username","email","id"]
    
    class Meta:
        model = Profile
    
admin.site.register(Status, StatusModelAdmin)
admin.site.register(Profile, ProfileModelAdmin)