from django.contrib import admin
from django.urls import path, include
from mypage import views
from mypage.views import add_status, add_profile, validate_form, logout, get_profile_list, delete_profile, favorite, get_books
from mypage.views import index, profile, books, search_book, registration, subscribers
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [    
    path(r'index/', index, name = 'index'),
    path(r'registration/', registration, name = 'registration'),
    path(r'profile/', profile, name = 'profile'),
    path(r'subscribers/', subscribers, name = 'subscribers'),
    path(r'books/', books, name = 'books'),
    path('search_book/<text>/', search_book, name='search_book'),
    url(r'^$', index, name='index'),
    url(r'add_status/$', add_status, name='add_status'),
    url(r'logout/$', logout, name='logout'),
    url(r'favorite/$', favorite, name='favorite'),
    url(r'get_books/$', get_books, name='get_books'),
    url(r'add_profile/$', add_profile, name='add_profile'),
    url(r'^ajax/validate_form/$', validate_form, name='validate_form'),
    url(r'get_profile_list/$', get_profile_list, name='get_profile_list'),
    url(r'delete_profile/$', delete_profile, name='delete_profile'),
]

urlpatterns += staticfiles_urlpatterns()