from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Status_Form, Profile_Form
from .models import Status, Profile
from django.contrib.auth import logout as logout_user
from validate_email import validate_email
import json
import requests

# Create your views here.

def index(request):
	all_status = Status.objects.all()
	form = Status_Form
	return render(request, 'index.html', {'Status': all_status, 'form': form, 'nbar': 'index'})

def subscribers(request):
    return render(request, 'subscribers.html', {'nbar': 'subscribers'})

def get_profile_list(request):
    profile_list = Profile.objects.all().values('username', 'email')
    profiles = list(profile_list)
    return JsonResponse(profiles, safe=False)

def delete_profile(request):
    username = request.GET.get('username', None)
    email = request.GET.get('email', None)
    password = request.GET.get('password', None)
    profile = Profile.objects.filter(email=email,username=username,password=password)
    isvalid = profile.exists()
    if isvalid:
        profile.delete()
    data = {"isvalid":isvalid}
    
    return JsonResponse(data)

def profile(request):
	return render(request, 'profile.html', {'nbar': 'profile'})

def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/books/')

def books(request):
    return render(request, 'books.html', {'nbar': 'books'})

def registration(request):
	form = Profile_Form
	return render(request, 'registration.html', {'form': form, 'nbar': 'registration'})

def validate_form(request):
    username = request.GET.get('username', None)
    email = request.GET.get('email', None)
    password = request.GET.get('password', None)
    confirm_password = request.GET.get('confirm_password', None)
    
    v_email = True if (validate_email(email) and not Profile.objects.filter(email=email).exists()) else False
    v_password = True if (password == confirm_password) else False
    v_all = v_email and email and username and password and confirm_password and v_password
    
    data = {
        'is_available': v_email,
        'is_email_invalid': not validate_email(email),
        'is_equal': v_password,
        'is_submitable': v_all
    }
    
    return JsonResponse(data)

def add_profile(request):
    form = Profile_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        email = request.POST.get('email', False)
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        confirm_password = request.POST.get('confirm_password', False)

        profile = Profile(email=email,
                          username=username,
                          password=password,
                          confirm_password=confirm_password)
        
        profile.save()
        return redirect(registration)
    else:
        return HttpResponseRedirect('/registration/')

def search_book(request, text=None):
    url = 'https://www.googleapis.com/books/v1/volumes?q='+text
    return JsonResponse(requests.get(url).json())

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		text = request.POST.get('text', False)
		text = Status(text=text)
		text.save()
		return redirect(index)
	else:
		return HttpResponseRedirect('/mypage:index/')
    
def get_books(request):
    if( not ('books' in request.session)):
        request.session['books'] = []
        
    return JsonResponse({'books': request.session['books']})
    
def favorite(request):
    added = request.GET.get('added', None)
    id = request.GET.get('id', None)
    
    if( not ('books' in request.session)):
        request.session['books'] = []
        
    if( not ('length' in request.session)):
        request.session['length'] = 0
    
    books = request.session['books']
    
    if(added == 'true'):
        if(not (id in books)):
            books.append(id)
    else:
        if(id in books):
            books.remove(id)
            
    request.session['books'] = books
    request.session['length'] = len(books)
    
    return JsonResponse({'length': request.session['length'], 'books': request.session['books']})