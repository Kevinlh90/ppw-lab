from django.db import models

# Create your models here.
class Status(models.Model):
    text = models.TextField()
    
class Profile(models.Model):
    email = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    confirm_password = models.CharField(max_length=200)