var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 500);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}

$(document).ready(function() {
    var count = 0;

    $('#button-theme-changer').click(function(e) {
        count = ++count % 3;
        var arrTheme;
        var url_img;
        var arrThemeOrigin = [{
            color:"#fbd408",
            color_hover:"#ffaa00",
            url_img: (typeof abstract_default === 'undefined') ? url_img : abstract_default,
            font_family:"-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue",
        }];
        var arrThemeBlack = [{
            color:"black",
            color_hover:"#42484d",
            url_img: (typeof abstract_black === 'undefined') ? url_img : abstract_black,
            font_family:"Lucida Console, Monaco, monospace",
        }];
        var arrThemeBlue = [{
            color:"#22b7e5",
            color_hover:"#218be4",
            url_img: (typeof abstract_blue === 'undefined') ? url_img : abstract_blue,
            font_family:"Palatino Linotype, Book Antiqua, Palatino",
        }];

        if(count == 1){
            arrTheme = arrThemeBlack;
        } else if(count == 2) {
            arrTheme = arrThemeBlue;
        } else {
            arrTheme = arrThemeOrigin;
        }

        
        $('header').css('background-image', arrTheme[0].url_img); 
        $('body').css('font-family', arrTheme[0].font_family);
        $('.bg-theme').css('background-color', arrTheme[0].color);
        $('.date-activity span').css('color', arrTheme[0].color_hover);
        $('#button-input').css('color', arrTheme[0].color);
        $('#button-input').css('border-color', arrTheme[0].color);
        $('#button-input').mouseover(function() {
            $(this).css("background-color",arrTheme[0].color_hover);
            $(this).css("color","white");
        }).mouseout(function() {
            $(this).css("background-color","transparent");
            $(this).css("color",arrTheme[0].color);
        });
        $('.accordion ul li a.toggle').css('background', arrTheme[0].color);
        $('.accordion ul li a.toggle').mouseover(function() {
            $(this).css("background",arrTheme[0].color_hover);
        }).mouseout(function() {
            $(this).css("background",arrTheme[0].color);
        });
    });
    
    $(".toggle").click(function(e) {
        e.preventDefault();
        
        var $this = $(this);
        
        if ($this.next().hasClass("show")) {
            $this.next().removeClass("show");
            $this.next().slideUp(350);
        } else {
            $this
            .parent()
            .parent()
            .find("li .inner")
            .removeClass("show").slideUp(350);

            $this
            .parent()
            .parent()
            .find("li .inner");
            $this.next().toggleClass("show");
            $this.next().slideToggle(350);
        }
    });
    
    var $input = $('.form-fieldset > input');

    $input.blur(function (e) {
        $(this).toggleClass('filled', !!$(this).val());
    });
});